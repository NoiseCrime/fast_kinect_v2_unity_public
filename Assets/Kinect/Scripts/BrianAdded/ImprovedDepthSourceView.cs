﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ImprovedDepthSourceView : MonoBehaviour {
    public GameObject depthFrameRenderer;
    private static ImprovedDepthSourceView instance;
    public static ImprovedDepthSourceView Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject();
                go.name = "_ImprovedDepthSourceView(Singleton)";
                instance = go.AddComponent<ImprovedDepthSourceView>();
            }
            return instance;
        }
    }
  

    void Awake()
    {
        //if it's null or if it's the right instance, don't destroy it.
        if (instance == null || instance == this)
            instance = this;
        else
        {
            DestroyImmediate(this);
        }
    }


	// Use this for initialization
	void Start () {
        depthFrameRenderer.renderer.material.mainTexture = DepthSourceManager.Instance.GetDepthFrameTexture();
	}

}
